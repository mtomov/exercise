require "dry/monads"
require "dry/matcher/result_matcher"

class BitmapEditor
  FIRST_OPERATION_ERROR_MESSAGE = "First operation must create the identity image, please re-run with first operation I"
  WHITE_COLOR = "O"

  def run(file)
    return puts "please provide correct file" if file.nil? || !File.exist?(file)

    file = File.open(file)
    BitmapProcessor.new.call(file)
  end


  Bitmap = Struct.new(:dots) do
    def initialize(dots = nil)
      super
      self.dots = dots || []
    end

    def to_s
      dots.map { |row|
        row.join
      }.join("\n")
    end

    def [](key)
      dots[key]
    end
  end


  class BitmapProcessor
    class << self
      attr_accessor :line_operators
    end

    self.line_operators = []

    def call(stream)
      bitmap = Bitmap.new

      unless CreateOperator.matches?(stream.first.chomp)
        puts FIRST_OPERATION_ERROR_MESSAGE
        return bitmap
      end

      stream.rewind

      stream.each do |line|
        line = line.chomp

        matching_operator = self.class.line_operators.find { |operator|
          operator.matches? line
        }

        next unless matching_operator

        matching_operator.new.call(bitmap, cmd: line) do |m|
          m.success do |new_bitmap, output|
            puts output if output
            bitmap = new_bitmap
          end

          m.failure do |message|
            puts message
          end
        end
      end

      bitmap
    end
  end


  class BaseOperator
    include Dry::Monads[:result]
  end

  class CreateOperator < BaseOperator
    include Dry::Matcher.for(:call, with: Dry::Matcher::ResultMatcher)

    def self.matches?(cmd)
      cmd.to_s[0] == "I"
    end

    def call(bitmap, cmd:)
      params = cmd.split(" ").map(&:chomp)
      y = params.pop.to_i
      x = params.pop.to_i

      y.times do
        bitmap.dots << Array.new(x, BitmapEditor::WHITE_COLOR)
      end

      Success(bitmap)
    end
  end
  BitmapProcessor.line_operators << CreateOperator


  class ShowCurrentImageOperator < BaseOperator
    include Dry::Matcher.for(:call, with: Dry::Matcher::ResultMatcher)

    def self.matches?(cmd)
      cmd.to_s[0] == "S"
    end

    def call(bitmap, cmd:)
      Success([bitmap, bitmap.to_s])
    end
  end
  BitmapProcessor.line_operators << ShowCurrentImageOperator


  class ColorAPixelOperator < BaseOperator
    include Dry::Matcher.for(:call, with: Dry::Matcher::ResultMatcher)

    def self.matches?(cmd)
      cmd.to_s[0] == "L"
    end

    def call(bitmap, cmd:)
      params = cmd.split(" ").map(&:chomp)
      color = params.pop
      y = params.pop.to_i - 1
      x = params.pop.to_i - 1

      return Failure(:pixel_not_found) if bitmap[y].nil?
      return Failure(:pixel_not_found) if bitmap[y][x].nil?

      bitmap[y][x] = color
      Success(bitmap)
    end
  end
  BitmapProcessor.line_operators << ColorAPixelOperator


  class DrawVerticalSegmentOperator < BaseOperator
    include Dry::Matcher.for(:call, with: Dry::Matcher::ResultMatcher)

    def self.matches?(cmd)
      cmd.to_s[0] == "V"
    end

    def call(bitmap, cmd:)
      params = cmd.split(" ").map(&:chomp)
      color = params.pop
      y2 = params.pop.to_i - 1
      y1 = params.pop.to_i - 1
      x = params.pop.to_i - 1

      return Failure(:area_out_of_bounds) if bitmap[y1].nil?
      return Failure(:area_out_of_bounds) if bitmap[y2].nil?
      return Failure(:area_out_of_bounds) if bitmap[y1][x].nil?
      return Failure(:area_out_of_bounds) if bitmap[y2][x].nil?

      y1.upto(y2) do |y|
        bitmap[y][x] = color
      end

      Success(bitmap)
    end
  end
  BitmapProcessor.line_operators << DrawVerticalSegmentOperator


  class DrawHorizontalSegmentOperator < BaseOperator
    include Dry::Matcher.for(:call, with: Dry::Matcher::ResultMatcher)

    def self.matches?(cmd)
      cmd.to_s[0] == "H"
    end

    def call(bitmap, cmd:)
      params = cmd.split(" ").map(&:chomp)
      color = params.pop
      y = params.pop.to_i - 1
      x2 = params.pop.to_i - 1
      x1 = params.pop.to_i - 1

      return Failure(:area_out_of_bounds) if bitmap[y].nil?
      return Failure(:area_out_of_bounds) if bitmap[y][x1].nil?
      return Failure(:area_out_of_bounds) if bitmap[y][x2].nil?

      x1.upto(x2) do |x|
        bitmap[y][x] = color
      end

      Success(bitmap)
    end
  end
  BitmapProcessor.line_operators << DrawHorizontalSegmentOperator
end
