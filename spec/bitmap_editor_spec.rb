require "spec_helper"
require "bitmap_editor"

describe BitmapEditor do
  let(:file) { File.join(Dir.pwd, "spec", "fixtures", "file.txt") }

  describe "#run" do
    context "when all is good" do
      it "runs without errors" do
        subject.run(file)
      end
    end

    context "when file is missing" do
      it "runs tells you so" do
        expect { subject.run("non-existant-file.txt") }.to output(/provide correct file/).to_stdout
      end
    end
  end
end

describe BitmapEditor::BitmapProcessor do
  describe "#line_operators" do
    it "returns the list of line operator classes" do
      expect(described_class.line_operators).to be_kind_of(Array)
    end
  end

  describe "#call" do
    let(:input) { StringIO.new("one\n") }

    context "when first operation is a create ( I )" do
      it "returns a new bitmap" do
        expect(subject.call(input)).to be_kind_of(BitmapEditor::Bitmap)
      end

      context "with a custom operator" do
        before do
          allow(BitmapEditor::CreateOperator).to receive(:matches?).with("one") { true }
        end

        let(:test_operator) do
          Class.new(BitmapEditor::BaseOperator) do
            include Dry::Matcher.for(:call, with: Dry::Matcher::ResultMatcher)

            def self.matches?(_cmd)
              true
            end

            def call(bitmap, cmd:)
              Success("modified-bitmap")
            end
          end
        end

        before do
          @original_operators = described_class.line_operators.dup
          described_class.line_operators = [test_operator]
        end

        after do
          described_class.line_operators = @original_operators
        end

        it "returns the modified bitmap from the custom operator" do
          expect(subject.call(input)).to eq("modified-bitmap")
        end
      end
    end

    context "when first operation is not a create ( I )" do
      let(:input) { ["one", "two"] }

      it "tells you so, and aborts" do
        expect(BitmapEditor::CreateOperator).to receive(:matches?).once.with("one") { false }

        expect { subject.call(input) }.to output(BitmapEditor::FIRST_OPERATION_ERROR_MESSAGE + "\n").to_stdout
      end
    end
  end
end

describe BitmapEditor::CreateOperator do
  describe ".matches?" do
    let(:input) { "I " }
    subject { described_class.matches?(input) }

    context "when first letter is I" do
      it { is_expected.to be true }
    end

    context "when first letter is anything else" do
      let(:input) { "" }
      it { is_expected.to be false }
    end

    context "when input is nil" do
      let(:input) { nil }
      it { is_expected.to be false }
    end
  end

  describe "#call" do
    let(:cmd) { "I   2  3 " }
    let(:bitmap) { BitmapEditor::Bitmap.new }

    it "has dry matcher behaviour" do
      result = subject.call(bitmap, cmd: cmd) { |m|
        m.success { |value| value }
        m.failure { raise }
      }
      expect(result).to be_kind_of(BitmapEditor::Bitmap)
    end

    it "creates a NxM dots image with an initial color white ( O )" do
      result = subject.call(bitmap, cmd: cmd)

      expect(result).to be_kind_of(Dry::Monads::Result::Success)
      result_bitmap = result.success
      expect(result_bitmap).to be_kind_of(BitmapEditor::Bitmap)
      expect(result_bitmap.to_s).to eq("OO\nOO\nOO")
    end
  end
end

describe BitmapEditor::ShowCurrentImageOperator do
  describe ".matches?" do
    let(:input) { "S " }
    subject { described_class.matches?(input) }

    context "when first letter is S" do
      it { is_expected.to be true }
    end

    context "when first letter is anything else" do
      let(:input) { "" }
      it { is_expected.to be false }
    end
  end

  describe "#call" do
    let(:cmd) { "S" }
    let(:bitmap) { BitmapEditor::Bitmap.new([[1, 2, 3], [4, 5, 6]]) }

    it "has dry matcher behaviour" do
      result = subject.call(bitmap, cmd: cmd) { |m|
        m.success { |value, output| [value, output] }
        m.failure { raise }
      }
      expect(result[0]).to be_kind_of(BitmapEditor::Bitmap)
      expect(result[1]).to be_kind_of(String)
    end

    it "prints the image to stdout" do
      result = subject.call(bitmap, cmd: cmd)

      expect(result).to be_kind_of(Dry::Monads::Result::Success)
      expect(result.success[0]).to be_kind_of(BitmapEditor::Bitmap)
      expect(result.success[1]).to eq "123\n456"
    end
  end
end

describe BitmapEditor::ColorAPixelOperator do
  describe ".matches?" do
    let(:input) { "L " }
    subject { described_class.matches?(input) }

    context "when first letter is L" do
      it { is_expected.to be true }
    end

    context "when first letter is anything else" do
      let(:input) { "" }
      it { is_expected.to be false }
    end
  end

  describe "#call" do
    let(:bitmap) { BitmapEditor::Bitmap.new([[1, 2, 3], [4, 5, 6]]) }
    let(:cmd) { "L 1 1 C" }

    it "has dry matcher behaviour" do
      result = subject.call(bitmap, cmd: cmd) { |m|
        m.success { |value| value }
        m.failure { raise }
      }
      expect(result).to be_kind_of(BitmapEditor::Bitmap)
    end

    context "targetting the first pixel" do
      it "colors it" do
        result = subject.call(bitmap, cmd: cmd)
        expect(result).to be_kind_of(Dry::Monads::Result::Success)
        expect(result.success.to_s).to eq "C23\n456"
      end
    end

    context "targetting the last pixel" do
      let(:cmd) { "L 3 2 C" }

      it "colors it" do
        result = subject.call(bitmap, cmd: cmd)
        expect(result).to be_kind_of(Dry::Monads::Result::Success)
        expect(result.success.to_s).to eq "123\n45C"
      end
    end

    context "targetting a non-existant row pixel" do
      let(:cmd) { "L 4 1 C" }

      it "returns a failure monad" do
        result = subject.call(bitmap, cmd: cmd)
        expect(result).to be_kind_of(Dry::Monads::Result::Failure)
        expect(result.failure).to eq(:pixel_not_found)
      end
    end

    context "targetting a non-existant column pixel" do
      let(:cmd) { "L 1 4 C" }

      it "returns a failure monad" do
        result = subject.call(bitmap, cmd: cmd)
        expect(result).to be_kind_of(Dry::Monads::Result::Failure)
        expect(result.failure).to eq(:pixel_not_found)
      end
    end
  end
end

describe BitmapEditor::DrawVerticalSegmentOperator do
  describe ".matches?" do
    let(:input) { "V " }
    subject { described_class.matches?(input) }

    context "when first letter is V" do
      it { is_expected.to be true }
    end

    context "when first letter is anything else" do
      let(:input) { "" }
      it { is_expected.to be false }
    end
  end

  describe "#call" do
    let(:bitmap) { BitmapEditor::Bitmap.new([[1, 2, 3], [4, 5, 6], [7, 8, 9]]) }
    let(:cmd) { "V 1 1 3 C" }

    it "has dry matcher behaviour" do
      result = subject.call(bitmap, cmd: cmd) { |m|
        m.success { |value| value }
        m.failure { raise }
      }
      expect(result).to be_kind_of(BitmapEditor::Bitmap)
    end

    context "targetting the first column" do
      it "colors it" do
        result = subject.call(bitmap, cmd: cmd)
        expect(result).to be_kind_of(Dry::Monads::Result::Success)
        expect(result.success.to_s).to eq("C23\nC56\nC89")
      end
    end

    context "targetting the last column" do
      let(:cmd) { "V 3 1 3 C" }

      it "colors it" do
        result = subject.call(bitmap, cmd: cmd)
        expect(result).to be_kind_of(Dry::Monads::Result::Success)
        expect(result.success.to_s).to eq("12C\n45C\n78C")
      end
    end

    describe "error scenarious" do
      context "targetting an out-of-bound column" do
        let(:cmd) { "V 4 1 3 C" }

        it "returns a failure monad" do
          result = subject.call(bitmap, cmd: cmd)
          expect(result).to be_kind_of(Dry::Monads::Result::Failure)
          expect(result.failure).to eq(:area_out_of_bounds)
        end
      end

      context "targetting an out-of-bound Y1" do
        let(:cmd) { "V 1 6 2 C" }

        it "returns a failure monad" do
          result = subject.call(bitmap, cmd: cmd)
          expect(result).to be_kind_of(Dry::Monads::Result::Failure)
          expect(result.failure).to eq(:area_out_of_bounds)
        end
      end

      context "targetting an out-of-bound Y2" do
        let(:cmd) { "V 1 1 6 C" }

        it "returns a failure monad" do
          result = subject.call(bitmap, cmd: cmd)
          expect(result).to be_kind_of(Dry::Monads::Result::Failure)
          expect(result.failure).to eq(:area_out_of_bounds)
        end
      end
    end
  end
end

describe BitmapEditor::DrawHorizontalSegmentOperator do
  describe ".matches?" do
    let(:input) { "H " }
    subject { described_class.matches?(input) }

    context "when first letter is H" do
      it { is_expected.to be true }
    end

    context "when first letter is anything else" do
      let(:input) { "" }
      it { is_expected.to be false }
    end
  end

  describe "#call" do
    let(:bitmap) { BitmapEditor::Bitmap.new([[1, 2, 3], [4, 5, 6], [7, 8, 9]]) }
    let(:cmd) { "H 1 3 1 C" }

    it "has dry matcher behaviour" do
      result = subject.call(bitmap, cmd: cmd) { |m|
        m.success { |value| value }
        m.failure { raise }
      }
      expect(result).to be_kind_of(BitmapEditor::Bitmap)
    end

    context "targetting the first row" do
      it "colors it" do
        result = subject.call(bitmap, cmd: cmd)
        expect(result).to be_kind_of(Dry::Monads::Result::Success)
        expect(result.success.to_s).to eq("CCC\n456\n789")
      end
    end

    context "targetting the last row" do
      let(:cmd) { "H 1 3 3 C" }

      it "colors it" do
        result = subject.call(bitmap, cmd: cmd)
        expect(result).to be_kind_of(Dry::Monads::Result::Success)
        expect(result.success.to_s).to eq("123\n456\nCCC")
      end
    end

    describe "error scenarious" do
      context "targetting an out-of-bound row" do
        let(:cmd) { "H 1 3 4 C" }

        it "returns a failure monad" do
          result = subject.call(bitmap, cmd: cmd)
          expect(result).to be_kind_of(Dry::Monads::Result::Failure)
          expect(result.failure).to eq(:area_out_of_bounds)
        end
      end

      context "targetting an out-of-bound X1" do
        let(:cmd) { "H 6 2 1 C" }

        it "returns a failure monad" do
          result = subject.call(bitmap, cmd: cmd)
          expect(result).to be_kind_of(Dry::Monads::Result::Failure)
          expect(result.failure).to eq(:area_out_of_bounds)
        end
      end

      context "targetting an out-of-bound X2" do
        let(:cmd) { "H 1 6 1 C" }

        it "returns a failure monad" do
          result = subject.call(bitmap, cmd: cmd)
          expect(result).to be_kind_of(Dry::Monads::Result::Failure)
          expect(result.failure).to eq(:area_out_of_bounds)
        end
      end
    end
  end
end
